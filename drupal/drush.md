---
title: Using Drush
description: "Drush makes Drupal maintenance simple. On our managed hosting platform, you can easily run Drush jobs via the shell login box (included free of charge)."
layout: default
parent: Drupal
status: published
categories:
  - drupal
tags:
  - drupal
  - drush
---

# Using Drush

## Selecting the PHP version to execute Drush

Since freistilbox provides several PHP versions, a little additional setup is required to make sure the correct version of the `php` command line interpreter is used for executing Drush and its sub-processes.

Please refer to [Using the correct PHP CLI version][php_version] for the details.

## How to create a site alias

Since you have access to a dedicated shell login host, using Drush for Drupal
maintenance is easy.

Here is a minimal `aliases.drushrc.php`:

```php
<?php
$aliases['s123'] = array(
  'uri' => 's123.cXX.freistilbox.net',
  'root' => '/srv/www/freistilbox/home/s123/current/docroot',
  'remote-host' => 'cXXs.freistilbox.net',
  'remote-user' => 's123',
);
```

As always, replace the parts for site ID ("s123") and shell login host ("cXXs")
with the actual values corresponding with your website instance.

## How to query the actual settings in Drupal 8

Neither the Drupal 8 admin interface nor the Drush `cget` command will by
default reflect configuration settings made in include files such as our
[configuration snippets](/how_it_works/includes):

```bash
$ drush cget syslog.settings
identity: drupal
facility: '128'
...
```

To get the actual value of a configuration setting, use the
`--include-overridden` switch:

```bash
$ drush cget syslog.settings --include-overridden
identity: s1503
facility: '128'
...
```

[php_version]: /how_it_works/php_versions/#using-the-correct-php-cli-version
