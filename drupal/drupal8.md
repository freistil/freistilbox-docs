---
title: How do the changes in Drupal 8 affect freistilbox?
description: "Our managed hosting platform is perfectly suited for Drupal 8. This article explains the changes to how it handled Drupal 7."
layout: default
parent: Drupal
categories:
  - drupal
tags:
  - drupal
---

# How to run Drupal 8 on freistilbox

Drupal 8 is a huge step forward and it'll be great to see what our customers get
to build on the long list of improvements that came with the new release.
Hosting Drupal 8 on freistilbox is straightforward. Only a few changes in the
new version affect the hosting layer and in this article, we cover everything
you need to know.

## freistilbox configuration snippets

We strongly advise using the Drupal 8 versions of our ready-made [configuration
snippets](/how_it_works/includes). They make sure features of Drupal 8 like the
Twig template cache work in an optimal way.

## Configuration management

Drupal 8 has the ability to export its settings from the application database to
a set of text files. And it can also update its active configuration from
changes made to the configuration file set on disk. Since your web application
creates these configuration files on-the-fly, you can not include them in the
Git repository along with your application code. The right location for these
files is the private section in the shared file storage where your boxes alone
can read and update them.

### Drupal 8.1.x

```php
$config_directories['sync'] = '../private/config/sync';
```

### Drupal 8.0.x

```php
$config_directories['active'] = '../private/config/active';
$config_directories['staging'] = '../private/config/staging';
```
