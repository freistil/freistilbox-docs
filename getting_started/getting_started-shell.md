---
title: "Shell access"
description: "Shell access is included in our managed hosting platform free of charge. This part of our Getting Started tutorial explains the details."
layout: default
parent: Getting started
status: published
categories:
  - getting_started
tags:
  - shell
  - ssh
  - sftp
nav_order: 9
---

# Shell access for maintenance and troubleshooting

Most web applications need to run regular maintenance tasks. From time to time,
site administrators also need to execute ad-hoc commands. On freistilbox all
that is easy because the hosting platform provides you with a separate login
shell for each site instance. In order to run maintenance jobs independent of
the boxes that deliver your application's web content, your freistilbox cluster
comes with its own dedicated shell server.

This shell server's hostname is `cNNs.freistilbox.net` where "NN" is the
numeric ID of your freistilbox cluster, for example `c42s.freistilbox.net`.

Every website instance has its own user account named after its site ID, for
example `s123`. Adding your public SSH key to a website instance on the
freistilbox Dashboard automatically gives you login access via its user
account.

## Using the login shell

Using the site ID as user name, you can access the shell box like this:

```shell
ssh s123@c42s.freistilbox.net
```

On the shell server, you'll find a complete web application environment with
your application's code and access to its asset files and configuration
snippets. In the directory `current`, your web application is deployed in the
same way as on the application boxes that run your website.

{: .warning }
The target to which `current` points will change during deployment. If you
actually need to update your application while you work in a shell session,
please make sure to `cd ~/current` again after pushing your changes.

## Transferring files via SFTP

Using the same user account, you can also access your website's asset files via
SFTP. This enables you to transfer files from and to your web application. Your
public shared files (directly available to external HTTP clients) can be found
under `current/docroot`, in the directories you defined as shared folders in
the [Boxfile][boxfile]. Your private shared files (only available to your
application) can be found in `current/private`.

## Shared storage space

The following command will show you the size of your cluster's shared storage
for media asset files and how much of it is currently available:

```bash
df -hT /glfs
```

## If you need help

We'll be happy to support you in solving any problem you have with running your
web application on the freistilbox hosting platform. You'll find all important
details on the [technical support][support] page.

[boxfile]: /how_it_works/boxfile
[support]: /faq/support
