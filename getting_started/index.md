---
title: "Getting started"
description: "If you're new to our managed hosting platform, this tutorial will tell you everything you need to get your website running."
layout: default
categories:
  - getting_started
has_children: true
nav_order: 2
---

# Getting started

In this tutorial, we'll guide you on your first steps using freistilbox.
Getting your first website launched on freistilbox will take you about an hour.
Over time, you'll get more routine and using freistilbox will become second
nature to you.

{: .warning }
You'll probably have so much fun using freistilbox that, after using it for a
few weeks, the thought of having to do things the old way again will make you
shudder.

*So let's get rolling!*
