---
title: "Configuring the Boxfile"
description: "This part of our Getting Started Tutorial explains how you can adapt our managed hosting platform to your application's needs."
layout: default
parent: Getting started
categories:
  - getting_started
tags:
  - boxfile
  - staging
  - storage
nav_order: 5
---

# The Boxfile

Whenever you synchronise the changes you made to your local repository with the
freistilbox Hosting Platform by doing a `git push`, you trigger the freistilbox
deployment process. This process updates the application code on each of your
boxes. It also does everything that's necessary to integrate your application
with the hosting stack. The details of this integration are defined in a
configuration file named [Boxfile][boxfile]. It tells freistilbox how to
integrate your application into the hosting environment.

## Location and format

The Boxfile is in YAML format and must be located in the topmost directory of
your application repository.

Because we have to adapt the Boxfile behaviour from time to time, we recommend
setting the Boxfile version explicitly:

```yaml
version: 2.0
```

{: .warning }
Changing the format version of an existing Boxfile will probably result in a
different deployment behaviour that needs to be addressed. Please refer to our
[detailed Boxfile documentation][boxfile] for the specifics.

## Environment-specific files

Modern software workflows include developing and testing in multiple [staging
environments][staging], and freistilbox supports these workflows. In order to
do so, it has to solve the following problem: All staging websites will be
deployed from the same Git repository but require different settings for the
database connection, among other things. That's why the Boxfile allows you to
declare certain files in your code base as specific for a single deployment
stage.

For starters, we'll only use the "production" stage, the website instance
that's aimed at the public. And to keep things simple, we'll declare only one
file as environment-specific: the main configuration file of your application.
Later, you can lean about all the details in the [Boxfile
documentation][boxfile].

### Drupal

Drupal's configuration by default lives in `sites/default/settings.php`. In the
`Boxfile`, we tell freistilbox that `docroot/sites/default/settings.php` is
going to vary between staging environments, and that in "production", we'd like
to have the file `settings.production.php` (in the same directory) take its
place:

```yaml
env_specific_files:
  docroot/sites/default/settings.php:
    production: settings.production.php
```

Notice that we won't have to repeat the full path for each environment-specific
file name; freistilbox assumes that all variants live in the same directory
together.

### WordPress

In a similar fashion, we can make the WordPress configuration file
environment-specific:

```yaml
env_specific_files:
  docroot/wp-config.php:
    production: wp-config.production.php
```

## Shared folders

The "shared" in "shared folders" means they're shared by all the servers that
deliver your website content on freistilbox. They're not shared between
different websites. That your web application runs on multiple servers at the
same time is one of the most important differences between freistilbox and
conventional hosting. While your code base is copied to each of these "boxes",
as we lovingly call them, asset files that are created by your application need
to be stored at a central place where they're available to all boxes. Where
these asset files are located in the file system differs from CMS to CMS and
from website to website. That's why the `Boxfile` lets you define these
locations yourself.

Let's take this Boxfile section:

```yaml
shared_folders:
  - docroot/sites/default/files
```

As you can tell from the path `sites/default/files`, this section is from a
`Boxfile` of a Drupal installation. With this configuration, the deployment
process will set up your application's file system in such a way that
everything under `sites/default/files` will reside on the shared file storage.

Armed with this knowledge, you should be able to guess how the `shared_folders`
section for a WordPress application should look like...

```yaml
shared_folders:
  - docroot/wp-content/uploads
```

{: .important }
The shared folders you declare in the `Boxfile` are created automatically by
our deployment process. That's why it's important that your application
repository __does not contain__ these directories. You can prevent them from
being added accidentally by telling Git to ignore them.

## Examples

This is how a basic Boxfile for Drupal would look like. It defines one shared
folder for asset files and two variants of `.htaccess` and application
configuration, one for the "production" environment and one for "testing":

```yaml
version: 2.0
shared_folders:
  - docroot/sites/default/files
env_specific_files:
  docroot/.htaccess:
    production: .htaccess.production
    testing: .htaccess.testing
  docroot/sites/default/settings.php:
    production: settings.production.php
    testing: settings.testing.php
```

And this is a version for WordPress:

```yaml
version: 2.0
shared_folders:
  - docroot/wp-content/uploads
env_specific_files:
  docroot/.htaccess:
    production: .htaccess.production
    testing: .htaccess.testing
  docroot/wp-config.php:
    production: wp-config.production.php
    testing: wp-config.testing.php
```

---

_Next:_ Let's set up the [platform services][gsservices] your web application
is going to need!

[boxfile]: /how_it_works/boxfile
[staging]: /how_it_works/staging
[gsservices]: /getting_started/getting_started-services
