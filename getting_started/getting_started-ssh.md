---
title: "Getting SSH access"
description: ""
layout: default
parent: Getting started
categories:
  - getting_started
tags:
  - ssh
nav_order: 2
---

# Setting up SSH access

For accessing freistilbox, you'll need an SSH key. SSH, "Secure Shell", is a
suite of tools that use an encrypted network protocol for transferring data.

## SSH Basics

There are SSH applications for every operating system:

* OpenSSH for Linux (use the distribution's package manager to install)
* OpenSSH for Mac OS X (pre-installed)
* [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/) for Windows

If you haven't used SSH before, you'll need to create your personal SSH key
first. This key will consist of two parts:

1. the public SSH key which you'll have to provide to freistilbox and
1. the private SSH key which you'll have to keep for yourself

You could view the public SSH key as an individual lock which we'll install for
you on your freistilbox website instance. Your private SSH key then is the key
to this lock, so you must keep that one stored securely on your work station
only. Accounts with your public key can only be accessed from computers where
your private key is installed.

As an additional security measure, you should strengthen your private key with a
passphrase that only you can guess. So even if someone got hold of your private
key, they'd have to know your passphrase as well to be able to use it.

Creating your SSH key is simple. You use the `ssh-keygen` command and provide it
with your email address. It'll ask you for the file in which to save your
private key; simply press "Enter" to accept the suggested location in your home
directory. Then enter your passphrase twice and you're done!

```text
$ ssh-keygen -C email@example.com
Generating public/private rsa key pair.
Enter file in which to save the key (/Users/foo/.ssh/id_rsa):
Enter passphrase (empty for no passphrase):
Enter same passphrase again:
Your identification has been saved in /Users/foo/.ssh/id_rsa.
Your public key has been saved in id_rsa.pub.
The key fingerprint is:
41:e2:07:cb:21:8d:3d:83:a0:87:f2:fb:6a:a8:45:a1 email@example.com
The key's randomart image is:
+--[ RSA 2048]----+
|  ...=+ .        |
| o  o+=*         |
|+ o   +oo        |
|.+ .   . .       |
|E o     S        |
| . .             |
| .o              |
|....             |
|o....            |
+-----------------+
```

(For creating an SSH key pair in Windows applications like PuTTY, please refer
to the application manual.)

This procedure will create two files, your _private key_ in a file named
`id_rsa` and your _public key_ in a file named `id_rsa.pub`. On Linux and
Mac OS, you will find them in the hidden subdirectory `.ssh` in your home
directory.

## Uploading an SSH key

To give you access, freistilbox only needs your _public_ key. Its contents
should look similar to this:

```plain
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDQk1gnrZLCoJzU9OYLRl4pbwR1GNkZ3B9O5SkFphyuu3m04O
2vRawjr6GLdMwzQd5NPLaIHP9+yHJdGxtHNGEGoocnw3dA2y3C0n0F/ yU8ckUueKoZyxFrdEqXD
T8w/ndJNGmoa0ST9rdYBMVY6EeCJPVj+xt4s7SwaxdpPybEEmJ2QTzqNKISXi3YJDa6np6i5uIl
+CcpwwFLzQXzpAwuGe1xzbIbInrHgK9pWLL1lpX3fbApqRucwIhXT2LsCNZLovGSDd/fIF/
pvaowqWBsyZgWN/bOtLbnqKWA9ur2JF8PUlqFWFB/YbbWJ6c5YF/QvL4OMp3AT4ZZwfhoKwZr
email@example.com
```

{: .important }
**Never share your _private_ SSH key with anyone!** Your private key must exist
securely on your workstation, and there alone.

Now let's upload your public key to freistilbox. Go to your user account
via the main menu:

![User account menu](/assets/images/dashboard_menu_user_account.png)

You can add one or more SSH keys by clicking the "Add key" button in the
section titled "SSH access". Copy and paste the contents of your public key
file into the text field and submit the form.

{: .notice }
If you have multiple SSH keys and would like to use a key different from your
default key, please see the FAQ ["How can I use a separate SSH key for
Git?"](/faq/git_ssh_key).

## Changing your SSH keys

Each time you add/remove an SSH key to/from your user account, freistilbox will make the necessary changes in the hosting infrastructure for each website to which you've been assigned as a [website user][accessmanagement].

---

_Next:_ You're all set! Time to [create our first website][gswebsite].

[gswebsite]: /getting_started/getting_started-website
[accessmanagement]: /how_it_works/access_management
