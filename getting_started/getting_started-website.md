---
title: "Your first website"
description: "This part of our Getting Started Tutorial explains how to create a new website instance on our managed hosting platform."
layout: default
parent: Getting started
categories:
  - getting_started
tags:
  - dashboard
nav_order: 3
---

# Your first website

## How to add a website to your freistilbox cluster

In the [freistilbox Dashboard](https://dashboard.freistilbox.com), select
the cluster to which you'd like to add your website and click "Create new
website".

![Adding a new website](/assets/images/dashboard_create_website.png)

freistilbox will create a new website for you and display its details on a separate page. This page is separated into different sections, each of which controls a specific aspect of the website's configuration.

## Adding collaborators

If you'd like to grant access to one or more users, you can add them in the "Users" section. You can assign each user one of the following roles:

- **Admin:** This user can make changes to the website details on the freistilbox dashboard.
- **Collaborator:** This user can view the website details on the freistilbox dashboard but not change them.

For every user assigned to the website, regardless of their role, freistilbox will deploy [their SSH keys][gsssh] to the website's [git repository][gsrepository] and [service box][gsshell].

You can find more on this topic in the article ["Access Management"][accessmgmnt]

---

_Next:_ Let's populate your application's [code repository][gsrepository]!

[gsrepository]: /getting_started/getting_started-repository
[gsssh]: /getting_started/getting_started-ssh
[gsshell]: /getting_started/getting_started-shell
[accessmgmnt]: /how_it_works/access_management
