---
title: "The code repository"
description: "This part of our Getting Started Tutorial explains how to set up Git for easy version control and deployment."
layout: default
parent: Getting started
categories:
  - getting_started
tags:
  - git
nav_order: 4
---

# Setting up your application repository

We've already made a point of describing the differences between a simple
hosting server and the distributed freistilbox architecture. It's because of
these differences that uploading your web application code to freistilbox works
differently, too. The common approach in conventional hosting, uploading your
Drupal or WordPress code to a single server via FTP or SFTP, just wouldn't work
well on freistilbox. Since freistilbox applications can run on any number of
boxes in parallel, we have to make sure that code updates get applied to all of
them at the same time. To make this as fast as possible, we'd have to use a
tool to track the actual few changes coming with an update and only apply these.

Instead of building this kind of tool ourselves, we chose an already existing
one that is proven to be robust, fast and reliable: the software version control
software Git. Git is built for the exact purpose of tracking changes in a code
base and distributing them quickly to remote locations. In other words, exactly
what we need! That's why every freistilbox web application lives in its own Git
repository.

{: .important }
Just to be completely clear, that means only the application code. Asset files
created by your application in production such as user uploads or log files, on
the other hand, are stored separately; we'll cover that later.

If you're not already familiar with Git, we recommend you get acquainted by
reading the [Git Tutorial](https://git-scm.com/docs/gittutorial).

{: .notice }
This tutorial assumes you have a basic familiarity with working in a terminal;
all examples here will be for a command-line shell.  You can use other tools
with a GUI, of course. Since we can't cover all possible alternatives, you'll
have to adapt the process described here to your tools of choice.

Now let's get started!

## Create the application repository

We'll begin by creating a copy of your website's Git repository from freistilbox
to your machine. Let's assume your website ID is "s007". The command to clone
your website repository into the local directory `mywebsite` will look
like this:

```shell
git clone s007@repo.freistilbox.net:s007 mywebsite
cd mywebsite
```

You can see that your website ID is used in two places of the command: first, as
the user name (in front of the "at" sign) and second, as the repository name
(after the colon).

{: .important }
Git uses SSH to access the freistilbox repository, so this command will only
work after you've added your [SSH key][gswebsite] on the freistilbox Dashboard.

## Import your application

The time has come to copy your application code into the repository. On
freistilbox, web applications live in the `docroot` folder below the repository
root. Simply create the `docroot` folder and copy the application there:

```shell
mkdir docroot
cp <application source directory>/* docroot/
```

---

_Next:_ Set up a basic [Boxfile][gsboxfile].

[gswebsite]: /getting_started/getting_started-website
[gsboxfile]: /getting_started/getting_started-boxfile
