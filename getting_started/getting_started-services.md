---
title: "Hosting services"
description: "Read our Getting Started tutorial on how to configure your application for the many backend services offered by our managed hosting platform."
layout: default
parent: Getting started
categories:
  - getting_started
tags:
  - database
nav_order: 6
---

# Hosting services

freistilbox provides you with all the hosting services that your application
needs for optimum performance, most importantly the database.

In the `current` directory, you'll find a subdirectory named `config` with
CMS-specific configuration snippets. You can include these snippets into your
web application settings to have important services configured automatically.

{: .warning }
If you put the includes at the beginning of your configuration file, their
settings may be overridden by statements further down below. This can either be
useful or confusing, depending on your point of view. We recommend appending
the `require_once` statement _at the end_ of your configuration file.

## Database configuration

Each freistilbox application can use one or more databases. To add a database,
simply click "Add database" in the "Databases" section for your web application:

![Adding a database to a website](/assets/images/dashboard_add_database.png)

The database list shows you every database available to your web application by
name and database host (add ".freistilbox.net" to get the fully qualified domain
name). If you want to see username and password, simply click on the entry. You
don't have to enter these credentials manually into your web app's
configuration, though.

For a Drupal application, simply append a single line to your
`settings.production.php`:

```php
require_once('../config/drupal/settings-d7-dbXXX.php');
```

Obviously, you need to replace "dbXXX" with the actual database name. If your
application uses a different Drupal version, replace "d7" with the appropriate
alternative (e.g. "d8" for Drupal 8).

At the time of writing, we don't yet provide configuration snippets for
WordPress. Simply [contact our support team][support] if you need help
configuring your WordPress application.

---

_Next:_ We've already mentioned our [deployment process][gsdeployment]. Let's take a closer look how it works!

[support]: /faq/support
[gsdeployment]: /getting_started/getting_started-deployment
