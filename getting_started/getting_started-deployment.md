---
title: "Deploying your code"
description: "Our managed hosting platform makes it perfectly simple to deploy application changes to production or to a staging environment."
layout: default
parent: Getting started
categories:
  - getting_started
tags:
  - deployment
  - git
nav_order: 7
---

# Deploy your web application

## Exclude files from deployment

While all your application code and configuration files belong in the Git
repository, there is a number of files and directories that you have to keep
separated. The most important example are the shared folders you've declared in
the [Boxfile][gsboxfile] such as `sites/default/files`; they're created by the
freistilbox deployment process and must not be deployed from your repository.
For the same reason, you need to also exclude environment-specific files, for
example `sites/default/settings.php`; only deploy their variants
(e.g. `settings.production.php`).

Fortunately, Git doesn't expect you to take extreme care with every commit. You
can simply exclude all the files you'd like to stay out of the repository by
listing them in a file named `.gitignore`, located in the repository's root
directory. You can use pattern matching to exclude a whole bunch of files.

The following would make for a good starter `.gitignore`:

```plain
*.bak

# Drupal
/docroot/sites/*/files
settings.php

# WordPress
/docroot/uploads
wp-config.php
```

See the [gitignore documentation](https://git-scm.com/docs/gitignore) for
details.

## Commit your changes

Now that you've set up your repository, it's time to collect all your local
changes and commit them to the repository as a new revision:

```bash
git add .
git commit
```

## Trigger the deployment

To deploy your Drupal installation to our freistilbox hosting platform, you
transfer all the changes you've made to the repository to the central
repository. You do this by using a simple command:

```plain
git push origin master
```

By doing a "push", you transfer all changes you've made since you've cloned or
updated your local repository from the central repository back to it.

A sophisticated deployment mechanism now takes care that all application
servers start to deliver this new version of your Drupal installation. It also
chooses the correct configuration file which most of the times will be
`settings.production.php`.

{: .warning }
Because Git is a distributed version control system, you can clone as many
repositories from the central one as you like -- for example in a development
team. You need to be careful, though, when you push changes from different
repositories. Since these repositories tend to diverge in their commit history,
pushing changes from different source repositories can lead to conflicts when
those changes need to be merged together on the freistilbox infrastructure.
Those conflicts then block all further updates and can only be resolved by our
tech support. That's why we recommend using a _central repository_ to first
integrate all changes from the different developer repositories and then push
everything at once to the hosting platform.

## Troubleshooting deployment

If something goes wrong during deployment, you will notice because your changes
won't become visible on the website. In that case, you should take a look at the
deployment logfile and see if it contains any error messages.

Use SSH to log in to the [remote access box][gsshell] and change into the directory `site/.deploy`. There, you'll find the deployment log file `deploy.log`:

```shell
cd site/.deploy
less deploy.log
```

If you need any help in getting deployment issues resolved, just [contact our
support team][support].

---

_Next:_ Your application is configured and ready to run. All that's missing is [setting up your application][gslaunch]!

<!-- TODO: Edit deployment chapter -->

[support]: /faq/support
[gsboxfile]: /getting_started/getting_started-boxfile
[gslaunch]: /getting_started/getting_started-launch
[gsshell]: /getting_started/getting_started-shell
