---
title: "Launching your website"
description: "This article explains how to make your website available to the public after you've deployed it to our managed hosting platform."
layout: default
parent: Getting started
categories:
  - getting_started
tags:
  - dns
  - domain
nav_order: 8
---

# Launching your website

Now that your website is deployed to our managed hosting platform, there are only a few steps left in order to make it publicly available!

## Set up DNS

For your website to respond to its domain name(s), their DNS records need to point to freistilbox. Please see our [DNS documentation][dns] for the details.

---

_Next:_ The final part of our tutorial -- How to use the login box for [maintenance and troubleshooting][gsshell]

[support]: /faq/support
[gsshell]: /getting_started//getting_started-shell
[dns]: /faq/dns
