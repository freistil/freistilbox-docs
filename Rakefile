# frozen_string_literal: true

require "html-proofer"

PROJECT_NAME = File.basename(Dir.getwd)

task default: :build

desc "Build the static website"
task :build do
  sh "bundle exec jekyll build"
end

desc "Run the full test suite"
task test: %i[build html_proofer]

desc "Check links and scripts"
task html_proofer: %i[build] do
  options = {
    assume_extension: true,
    disable_external: false,
    enforce_https: true,
    ignore_urls: [
      /https:\/\/www\.drupal\.org/,
      /https:\/\/(www\.T)?twitter.com/,
      /https:\/\/docs\.freistilbox\.com/,
      /https:\/\/www\.cleverreach\.com/,
    ],
    internal_domains: %w[docs.freistilbox.com],
  }
  HTMLProofer.check_directory("./_site", options).run
end

desc "Run a local dev server"
task :preview do
  sh "bundle exec jekyll serve -l"
end

namespace :docker do
  DOCKER_IMAGE = PROJECT_NAME

  desc "Build Docker image"
  task :build do
    sh "docker build -t #{DOCKER_IMAGE} ."
  end

  desc "Run container locally"
  task :run do
    sh "docker run --rm -p 5000:5000 -e DEV_ENV=true #{DOCKER_IMAGE}"
  end
end
