# frozen_string_literal: true

require "rack"
require "rack/contrib/try_static"
require "rack/rewrite"
require "rack/ssl"

# Enable proper HEAD responses
use Rack::Head

# Force SSL
puts "RACK_ENV: #{ENV['RACK_ENV']}"
if ENV["RACK_ENV"] == "production"
  use Rack::SSL
end

# Add basic auth if configured
if ENV["HTTP_USER"] && ENV["HTTP_PASSWORD"]
  use Rack::Auth::Basic, "Restricted Area" do |username, password|
    [username, password] == [ENV["HTTP_USER"], ENV["HTTP_PASSWORD"]]
  end
end

use Rack::Rewrite do
end

# Serve static files
use Rack::TryStatic,
    root: "_site",
    urls: %w[/],
    try: [".html", "index.html", "/index.html"],
    header_rules: [
      [:all, { "cache-control" => "public, max-age=3600" }],
      [%w[css js], { "cache-control" => "public, max-age=31536000" }],
      [%w[jpg png gif], { "cache-control" => "public, max-age=31536000" }],
    ]

# Serve a 404 page if all else fails
run lambda { |_env|
  [
    404,
    {
      "content-type" => "text/html",
      "cache-control" => "public, max-age=60",
    },
    File.open("_site/404.html", File::RDONLY),
  ]
}
