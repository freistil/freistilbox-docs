---
title: Which MySQL storage engine should I use?
description: "Choosing the right database storage engine is important for maximum website performance. This article explains the details."
layout: default
parent: Databases
categories:
  - tech_faq
tags:
  - mysql
  - database
---

# Creating tables with the InnoDB storage engine

When you create new tables for your application database, you should
definitively use InnoDB as the storage engine. InnoDB is in many aspects
superior to MyISAM and offers a multitude of tuning possibilities. InnoDB also
allows for consistent online backups without locking database access. Drupal 7
and up already uses InnoDB by default.

For these reasons, our hosting environment is optimized for InnoDB and we
support only the InnoDB storage engine. If your database schema still uses
MyISAM tables, you should convert them to InnoDB. We'll explain below how to
do that.

## Converting a database from MyISAM to InnoDB

There are at least three alternative ways to get rid of MyISAM-based tables.

### Use SQL

The most common way to do the data conversion is via the `mysql` command line
tool. Execute the following commands either locally on your development machine
(before creating a database dump and importing that on freistilbox) or directly
on the hosting platform via your [remote login](/getting_started/getting_started-shell/).

This is how you find all the tables that still use the MyISAM storage engine:

```sql
SELECT table_name FROM information_schema.tables WHERE table_schema = '<database_name>' AND engine != 'innodb';
```

Each database table in this list then needs to be converted to InnoDB with the
following command:

```sql
ALTER TABLE <table_name> ENGINE=INNODB;
```

### Use the DB Tuner module

With DB Tuner, there's a Drupal module that allows you (among other things) to
convert MySQL tables to InnoDB.

### Let our engineers do the conversion

Alternatively, we can convert your tables for you; simply send us a
[support request](/faq/support).
