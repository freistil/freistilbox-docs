---
title: Why are database IDs not continuous?
description: "Our managed hosting platform uses a high-availability setup for databases. This article explains the effects you might see in your application."
layout: default
parent: Databases
categories:
  - tech_faq
tags:
  - database
  - mysql
---

# Why database IDs have only odd (or only even) key values

You'll notice that on our freistilbox clusters, `auto_increment` primary keys, e.g. node
IDs, always grow by an increment of 2.

This is due to the data replication in our MySQL clusters. In order to prevent any kind of key collision between cluster nodes, one database node only assigns odd `auto_increment` numbers, the other one only even ones.
