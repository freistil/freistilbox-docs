# freistilbox User Documentation website

[![Netlify Status](https://api.netlify.com/api/v1/badges/74f4232d-f95b-4050-956b-f4ac268a1dc4/deploy-status)](https://app.netlify.com/sites/docs-freistilbox-com/deploys)

This website is based on [Jekyll](https://jekyllrb.com/) and uses the [Just The Docs theme](https://github.com/pmarsceill/just-the-docs).
