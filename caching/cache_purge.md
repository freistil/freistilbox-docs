---
title: How can I force an immediate cache refresh?
description: "Varnish caching is included free of charge in our managed hosting platform. This article explains how to update cache content on demand."
layout: default
parent: Caching
status: published
categories:
  - caching
tags:
  - varnish
  - cache
  - drush
---

# How to force Varnish to update cache contents

Every freistilbox cluster on our managed hosting platform has its own Varnish
content cache. This cache stores content according to the HTTP headers that came
with it. When the cache lifetime of a specific content object expires, Varnish
will remove it from its cache store and request an update from your web
application.

Triggering a cache update requires connecting to the active Varnish node. Even
though your freistilbox cluster might have more than one Varnish server
(especially if you have a freistilbox Pro oder Enterprise cluster), the hosting
platform maintains a hostname that always points to the active node. Its name
consists of your Cluster ID with a "v" for "Varnish" appended, for example
"c61v".

## Cache invalidation using the Drupal Purge module

In order to let your Drupal application delete stale cache content, freistilbox
supports the [Purge module](https://www.drupal.org/project/purge) in conjunction
with the [Varnish Purger](https://www.drupal.org/project/varnish_purge) module.

The Varnish Purger module allows invalidating whole sets of cache objects at
once by using "Cache Tags". This method uses the `Cache-Tags` HTTP header to
label content with one or more tags. All content with a certain tag can then be
removed from the cache using a `BAN` request for this tag.

Configure the Varnish Purger module using the following details:

* Hostname: `cXXv.freistilbox.net` (use your own cluster ID followed by a "v"
  for Varnish, for example `c61v.freistilbox.net`)
* Port: `80`
* Path: '/`
* Request method: `BAN`
* Scheme: `http`

Also make sure that the Varnish Purger Tags submodule is configured like this:

* Header: `Cache-Tags`
* Value: `[invalidation:expression]`

## Manual cache invalidation

To manually remove a certain content object from the cache, you can send a
`PURGE` request with its URL from the command line on your
[shell box](/getting_started/getting_started-shell/).

As an example, let's assume we want to refresh the cached content for the URL
`https://www.example.com/tralalaa.css`.

The following `curl` command sends a HTTP request to the active Varnish instance
of your cluster, using the document path we want to invalidate.  However,
instead of the usual `GET`, it uses the `PURGE` request method. It also includes
a `Host` header to identify your website's domain name.

```shell
curl -X PURGE http://c61v.freistilbox.net/tralalaa.css \
  -H "Host: www.example.com"
```

If your website enforces encryption by redirecting any `http://` URL to its
`https://` equivalent, you need to add another header named `X-Forwarded-Proto`.

```shell
curl -X PURGE http://c61v.freistilbox.net/tralalaa.css \
  -H "Host: www.example.com" -H "X-Forwarded-Proto: https"
```

After you've issued this command, Varnish will refresh the content for the given
URL.
