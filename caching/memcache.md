---
title: How do I enable memcached?
description: "Our managed hosting platform provides you with a Memcached application cache free of charge. This article explains how to set it up."
layout: default
parent: Caching
categories:
  - caching
tags:
  - memcache
  - configuration
---

# Speeding up your application with Memcached

## Using Memcached in Drupal

freistilbox provides you with a ready-to-run configuration file containing all
necessary Memcached settings. Simply include the file in your
`settings.production.php` and set the right caching module for your Drupal
version.

{: .important }
Make sure to set the right path to the `memcache.inc` file in your application
source tree.

## Drupal 8

Configuration example:

```php
<?php
...
require_once('../config/drupal/settings-d8-memcache.php');
```

The [memcache module](https://www.drupal.org/project/memcache) is currently
under heavy development for the 8.x update. We recommend using the latest
alpha if not the the development version.

## Drupal 7

Configuration example:

```php
<?php
...
require_once('../config/drupal/settings-d7-memcache.php');  
$conf['cache_backends'][] = 'sites/all/modules/contrib/memcache/memcache.inc';
```

## Drupal 6

Configuration example:

```php
<?php
...
require_once('../config/drupal/settings-d6-memcache.php');  
$conf['cache_inc'] = './sites/all/modules/contrib/memcache/memcache.inc';
```
