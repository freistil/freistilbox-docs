---
title: Memcached usage
description: "Our managed hosting platform provides you with a memcached application cache free of charge. This article explains how to use it."
layout: default
parent: Caching
categories:
  - caching
tags:
  - memcache
  - troubleshooting
---

# How to interact with memcached on freistilbox

On freistilbox, you have access to a dedicated memcache service for your cluster.
You can find its IP address in the config snippets we provide. These snippets are
placed under the siteuser's home directory in `current/config/drupal/` and are
called settings-d*-memcache.php, one for each drupal version - the IP address is
the same for each of them.

Please note that we firewall our memcached installations so that they're not
reachable through the internet. You can interact with it from the shellhost,
using a combination of `echo` and `netcat` commands.

## Get memcached statistics

Issue the following on your shellhost. Do not forget to replace 1.2.3.4 with your
actual memcached IP address:

```bash
echo stats | nc 1.2.3.4 11211
```

## Flush memcached objects

```bash
echo flush_all | nc 1.2.3.4 11211
```

## More memcached commands

Find here a [complete list of commands](https://github.com/memcached/memcached/wiki/Commands) you can send to memcached.

[caching_basics]: /docs/caching_basics
