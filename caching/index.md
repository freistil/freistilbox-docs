---
title: Caching
description: "An overview of the different caching solutions our managed hosting platform offers you free of charge."
layout: default
has_children: true
nav_order: 4
categories:
  - caching
tags:
  - cache
  - varnish
  - memcache
  - redis
  - opcache
---

# Caching

Caching is an important strategy for optimal website performance. Fast websites, especially if they are more complex, rely on caching technology. How does it work?

Let's explain it with an analogy. If you have a device that takes up a lot of space, it might be more efficient to store if you disassemble it first and put it into storage as single pieces. The downside is that when you need the device again, you'll have to put it together first. You're trading more effort for less storage space.

In web operations, we tend to go the opposite way. Instead of making our web application assemble content from data sources over and over again, we let it do it once and expend more storage space to retain the result ready for delivery on demand. This storing of application artefacts is called caching.

Caching has its weaknesses, too:

1. Cache capacity is always limited. We can't store everything under the sun.
2. If the artefact, a rendered web page for example, is dynamic in nature, what we serve from the cache might have become stale and not be identical anymore to what the application would generate at present.

Ageing cache content needs to get removed or replaced in time. That's why the invalidation of cache content is an important aspect. (It is also considered one of the [two hard things in computer science](https://martinfowler.com/bliki/TwoHardThings.html).) Most cache expiration algorithms use a variation of the Least-Recently-Used strategy (LRU) where the retention time of cache objects depends on how often they get reused.

freistilbox makes extensive use of caching technology wherever it is beneficial for system load and content delivery performance. Here is a list of the most important caching components you can use to speed up your Drupal or WordPress application:

1. **Content cache:** This service, based on a Varnish reverse proxy, receives all inbound HTTP requests. Only if it can not answer them with current cache content, it passes them on to one of your application boxes. The response it receives then gets cached for a time derived from its HTTP headers. Read our content cache page for more details.
2. **Application cache:** Before complex content like a web page can be delivered, your web application has to render its parts from information it gets from the database and other sources. Storing these artefacts in a cache can significantly reduce load on the application servers and the database. With Memcached and Redis, freistilbox offers two popular choices for caching artefacts.
3. **Opcode cache:** Even lower down in the depths of a web application, we'll find the recurring work of interpreting code and transforming it into "opcodes" that can be executed. In the PHP world, APC has for the most part been replaced by Opcache, which is fully supported by freistilbox.

The performance of both Drupal and WordPress benefits highly from efficient caching. With its distributed architecture, our managed hosting platform allows you to take advantage of caching at all levels of your web application.
