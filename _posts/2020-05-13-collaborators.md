---
layout: post
title: More powerful website access management
date: 2020-05-13 14:54 +0100
---

With today's launch of what we've internally called the "user-centric
dashboard", we finally resolved the tedious issue that giving users access
required adding their SSH keys to each new website over and over again.
Every user now maintains their personal list of SSH keys in their user
account. You grant them access to a website simply by referencing their
email address. This should reduce adding SSH keys directly to a website to
a rarely used feature.

This also means that you can add any user with the same ease, regardless if
they're a part of your company or not. There's no more need to create
additional dashboard user accounts for client clusters!

Please check out ["Managing User Access"](/how_it_works/access_management)
for the details.
