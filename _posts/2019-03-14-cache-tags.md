---
title: Varnish cache tags support
date: 2019-03-14
layout: post
---

* We now support granular Varnish Cache invalidation using `Cache-Tags` HTTP Header. Read more about this in our [Blog](https://www.freistil.it/cache-tags-support/) and the [user documentation on how to purge cache contents](/caching/cache_purge/).
