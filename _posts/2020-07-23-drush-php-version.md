---
layout: post
title: "Selecting the right PHP version for Drush"
date: 2020-07-23 14:58 +0100
---

We've added a decription of a simple workaround for making sure that [Drush](/drupal/drush) is run with the correct PHP version.
