---
title: "Boxfile parsing fix"
layout: post
date: 2017-08-01
---

* We've fixed a bug in the handling of YAML errors in the `Boxfile`. The aborted
  deployment then left stale lock files, preventing any further deployment. Now,
  Boxfile errors will be properly logged.
