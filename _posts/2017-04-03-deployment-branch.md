---
title: "Per-website deployment branch"
layout: post
date: 2017-04-03
---

* You can now assign a deployment branch for each individual application
  instance, making staging workflows more straightforward.
