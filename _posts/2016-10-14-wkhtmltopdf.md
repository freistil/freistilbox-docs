---
title: "wkhtml2pdf tool available"
date: 2016-10-14
layout: post
---

* Added changelog :)
* Installed [wkhtmltopdf](https://wkhtmltopdf.org) v0.12.2 on freistilbox. Its path is `/usr/local/bin/wkhtmltopdf`. There's also `wkhtmltoimage` to create images instead of PDF files.
