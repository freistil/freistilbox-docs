---
title: "New feature: Delete website"
layout: post
date: 2017-03-23
---

* We've added a big red button that lets you remove web applications from your
  cluster. At this time, they only go offline; content isn't deleted.
