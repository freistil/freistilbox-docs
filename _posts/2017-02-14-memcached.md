---
title: "Better Memcached support for Drupal 8"
layout: post
date: 2017-02-14
---

* We updated the memcache integration snippet for Drupal 8. See the [memcache snippet documentation](/caching/memcache) for the very brief instructions on how to use it.
