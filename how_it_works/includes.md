---
title: Configuration snippets
description: "Our managed hosting platform makes application setup easy by providing you with code snippets that you can simply include in your configuration files."
layout: default
parent: How it works
categories:
  - basics
tags:
  - database
  - memcache
---

# Ready-made configuration snippets

In some cases, it is helpful _or even required for proper website operation_ to
adapt your web application's configuration to the freistilbox hosting
environment. To simplify setting up this configuration, freistilbox provides
you with CMS-specific **configuration snippets that you should integrate into
your own application settings**.

Since these configuration snippets are stored in a subdirectory named `config`
on the same level as the `docroot` directory of your web application, you
simply include these snippets in your application configuration file using a
relative path like this:

```php
require_once('../config/«CMS»/«CONFIGFILE»');
```

Which configuration files are available for which CMS is laid out in detail
below.

{: .warning }
If you insert the `require_once` statement at the beginning of your
configuration file, you risk that its settings get overridden further down in
your configuration file, which can lead to unexpected behaviour. That's why we
recommend appending the `require_once` at the end of your configuration file.

## Drupal configuration

Drupal-specific configuration snippets are stored in the directory
`../config/drupal/`.

The following configuration files are available. In their file names, replace
`«version»` with a shortcut for the Drupal version the website uses.
freistilbox supports the shortcuts "d6", "d7" and "d8".

- `settings-«version»-site.php` -- General website settings, for example
  `reverse_proxy_addresses` and Twig cache directory
- `settings-«version»-dbXXX.php` -- Database configuration. Replace `dbXXX`
  with the database name as listed on the freistilbox Dashboard.
- `settings-«version»-memcache.php` -- Memcached configuration. See ["How do I
  enable memcached?"][memcached] for details.

There is also a `settings-«version»-site-beta.php` that extends the `site`
snippet with settings we make available ahead of global release.

The minimum configuration for running a Drupal website on freistilbox consists
of the `settings-...-site.php` and `settings-...-dbXXX.php` snippets. The
following example shows how to include them in a Drupal 8 website that is
connected to database `db123`.

```php
<?php
// Drupal 8 settings.php
...
// Include configuration snippets at the end
require_once('../config/drupal/settings-d8-site.php');
require_once('../config/drupal/settings-d8-db123.php');
?>
```

{: .notice }
Please note that the admin interface of your web application will not reflect
settings from these include files. If you need to check the actual
configuration values, take a look at the snippet files themselves or use
[Drush](/drupal/drush).

## WordPress configuration

At this time, we don't provide configuration snippets for WordPress. Please
[contact our support team][support] if you need help configuring your WordPress
application.

[support]: /faq/support
[deployment]: /how_it_works/deployment
[memcached]: /caching/memcache
