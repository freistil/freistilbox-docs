---
title: "PHP versions"
description: "Our managed hosting platform supports all current PHP versions."
layout: default
parent: How it works
status: published
categories:
  - basics
tags:
  - php
  - cron
---

# Selecting a PHP version

While older freistilbox setups only supported a single PHP version, customers on
newer plans can choose between multiple current PHP versions on a per-website
basis.

In your cluster's detail information, you'll find which versions it supports:

!["PHP versions available on a cluster"](/assets/images/dashboard_cluster_php_versions.png)

For each of your websites, you can then select one of these versions:

!["Choosing a PHP version"](/assets/images/dashboard_website_php_version.png)

## Using the correct PHP CLI version

On your [shell box][shell], you need to make sure to use the correct
version of `php` CLI interpreter. Since most customers need to use
different PHP versions between their websites, the shell box has packages
installed for all available PHP versions. Each package offers a versioned
name of the CLI interpreter, for example `php7.3` for PHP version 7.3.
Using a versioned binary works in simple cases but more often than not,
tools like [Drush][drush] will call the PHP interpreter via the
non-versioned `php` command.

This is why we managed the shell's environment for you as long as you are using
`bash` as the site's users CLI shell.

If you need to use a different version than configured in the dashboard or you've
switched to another shell than `bash` you need to export an environment variable
`PHP_VERSION` containing the version number for PHP you want to use.

In this example `drush` is going to use `php7.3` ignoring what is configured in the
dashboard:

```shell
export PHP_VERSION="7.3"
drush updatedb
```

## Using the correct PHP version in a cronjob

On the shell box you also have the possibility to create [cronjobs] to run 
commands regularly. Since cron doesn't load the shell environment automatically
like during a login session, you have to tell cron to use it.

Add these to lines to the beginning of the `crontab`:

```cron
SHELL=/bin/bash
BASH_ENV=~/.bashrc
```

[support]: /faq/support
[shell]: /getting_started/getting_started-shell
[drush]: /drupal/drush
[cronjobs]: /faq/cron
