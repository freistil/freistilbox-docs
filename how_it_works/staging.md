---
title: Staging environments
description: "Modern web development workflows use staging environments for tests and QA. Our managed hosting platform includes them free of charge."
parent: How it works
layout: default
categories:
  - basics
tags:
  - git
  - deployment
  - configuration
---

# Multi-stage testing on freistilbox

A modern application development workflow consists of multiple stages: When new changes are made, they're first tested on the developer's workstation. Before a feature is released in production, it commonly undergoes user acceptance testing in a staging environment. Since we'd like to test under production conditions, this staging environment should run on the same hosting platform as the production application.

Practicing this staged workflow on freistilbox is easy. Simply create a development or testing environment on your freistilbox cluster, deploy your changes and test them without any impact on your production application.

## Environment-specific configuration

There's one aspect that complicates things (but, as you'll see, only a bit): Different deployment environments will need different configuration settings. For example, each environment has its own database, and maybe you'd also like to have your staging environment shielded from the public by a login.

A simple but impractical approach would be to maintain a separate code repository for each deployment environment. For example, you'd have a "testing" repository that you'd push to the testing environment and a "production" repository that would be deployed to the production environment. The only difference between these repositories would be a few configuration files like `.htaccess` or `wp-config.php`, and you'd have to find a way to synchronise every application change across your repositories. Rather sooner than later, this fragile process would introduce exactly the mistakes that you wanted to prevent by using a staging workflow in the first place.

It's pretty obvious that the better alternative is working in a single repository. While you're developing a new release, you'll push changes to the testing environment and as soon as you're finished, you'll do a final push to the production environment. And of course, freistilbox supports you in doing that!

But how can your application use different settings depending on the stage in which it's being run? The answer is "environment-specific files".  In your [Boxfile][boxfile], you can declare files that will only be used in a specific deployment environment. That way, you can have a `settings.production.php` and a `settings.testing.php`, and freistilbox will automatically use the former in the production environment and the latter in the testing environment. Please see the [Boxfile documentation][boxfile] for details.

All you need to do in advance is assign the right label to each of your deployment environments. By default, all the application instances on your freistilbox cluster will be labeled "production". To assign a different label, simply change the application's "deployment environment" on the freistilbox Dashboard.

## Working with branches

Especially in teams working on different parts of the application at the same time, it has become common practice to diverge from the main line of development for new work by creating a new branch in the repository. While the "master" or "main" branch usually holds the stable production release, ongoing development work is usually done on separate branches. Every once in a while, you'll push your development branch to its separate deployment environment to see if everything is okay. And when you've reached a development milestone, you merge all the changes from your development branch back into "master" and push the result to the production environment.

freistilbox supports this development workflow and allows you to use different branches for different staging instances.

When you are on the details page of your production website, you can use the "Add staging instance" button in the "Staging" section to create a new staging website. This staging website will not have its own Git repository. Instead, it uses the Git repository of the production website, but deploys its code from a separate branch. This way, you can have your production website deploy from the "main" branch and your staging website deploy from a development branch. In the input form that pops up, simply name the development branch in the "Deployment environment" field and click "Create staging instance". This name will be assigned to both the deployment environment (which you use to define site-specific configuration settings) and the branch that will be deployed.

Here's how you set up your local Git repository for this workflow. After creating your staging instance, you simply create a new branch with the environment name you assigned to your staging instance. Then, you keep pushing changes the existing freistilbox repository, using the same name.

Let's assume as an example that your production website uses the "master" branch, and you've added a staging instance using the "testing" branch. With these settings, a `git push` to the "master" branch will update the production environment, and a push to the "testing" branch will deploy your staging instance.

```shell
git checkout -b testing     # create a new branch named "testing"
git push -u origin testing  # push the new branch to the freistilbox repository
```

{: .notice }
Using the `-u` option is necessary only once to link your local "testing" branch to the remote branch of the same name.

It will take a bit of time until you're accustomed to this very effective workflow. Don't feel bad if you get stuck, it happens to everyone. Simply get in touch with our [support team][support] and our engineers will help you out.

[boxfile]: /how_it_works/boxfile
[support]: /faq/support
