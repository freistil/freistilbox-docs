---
title: "Access management"
description: ""
layout: default
status: published
parent: How it works
categories:
  - basics
tags:
  - access
  - ssh
---

# Managing user access

When you're working on a website with other people, those people will need access to details of that website. They might even have to be able to make changes to its settings. The latter is especially the case if you're the owner of the website and have tasked someone else with its development and maintenance.

freistilbox allows you to provide other dashboard users with access to your website details. Each website maintains its own users list; you'll find it in the "Users" section on the website details page:

<!--TODO: screenshot-->

Here, you can add users and assign them one of the following access levels:

- **Collaborator:** This user can view the details of the website but will not be able to make any changes to them.
- **Admin:** This user can both view and modify the details of the website.

## SSH access

For all users, regardless of their access level, freistilbox will deploy the SSH keys stored in their dashboard user account both to the website's git repository as well as to the service box that allows developers [shell access][gsshell].

[gsshell]: /getting_started/getting_started-shell
