---
title: Troubleshooting deployments
description: "This article helps you identify and resolve deployment issues."
layout: default
parent: FAQ
categories:
  - tech_faq
tags:
  - deployment
---

# Troubleshooting deployments

It may sometimes happen that you push your code into our repository, but no
matter how long you wait, the changes seem not to reach the webserver.
There are many reasons why this can happen, this article shows you how you can
find out what happend and what to do about it.

## The `deploy.log` file

If you have trouble deploying your code, log in to the
[shell box][shellbox] and change the working directory to
`.deploy`. You'll find a couple of files and a directory there that shouldn't
matter much to you, but the `deploy.log` file that resides there can be an
interesting source for information about what went wrong during deployment.

Read below what you may find there, what it means and how you possibly can solve
it yourself, without having to wait for our support.

### `WARN -- : No Boxfile`

Depending on how you want to use your site, this may be perfectly okay.
If you're using asset files that you copy to the [shell box][shellbox] or if your cluster
has more than one webserver it is certainly not, see [Boxfile][boxfile].

### `FATAL -- : Could not create symlink FILE: File exists`

In most cases this means that you defined a shared folder in your `Boxfile`, but
you also checked that folder in along with your code. Our deployment system
then tries to create a symlink with the name of that folder, and can't do that
because the target already exists. The solution is to remove the folder from
the code repository and push again.

To remove the file or directory you can remove it from git repository tracking.

A common one is the `sites/default/files` directory in Drupal. Run these
commands to remove it from git, but keep your files locally so you can upload
them via SSH/SFTP to the [shell box][shellbox]:

```shell
echo docroot/sites/default/files >> .gitignore
$ git rm --cached docroot/sites/default/files
$ git commit docroot/sites/default/files .gitignore -m "Remove directory from git"
```

Push this change and the error shouldn't happen again.

### `FATAL -- : Lockfile exists`

This is very rare and indicates a bug in our deployment system. Due to the fact
that you don't have access to the web servers, it is unlikely that you can fix
this by youself. Use the dashboard to open a [support request][support] and provide us
with the last 5-10 lines of the `deploy.log`, we'll fix that.

### `ERROR -- : Environment-specific file FILE does not exist`

This happens when you made an entry into the [Boxfile][boxfile] that
points to a file our deployment system should create a symlink to, but that file
is not checked in along with the code. E.g., if you have a reference to
`htpasswd.staging` in your `Boxfile`, you must either check in that file or
remove the entry from the `Boxfile`.

### `FATAL -- : Failed changing attributes of FILE: Operation not permitted`

Another very rare case where filesystem permissions have changed in a way that
prevents the deployment system from working.  
Please open a [support request][support].

### `FATAL -- : Branch BRANCH does not exist.`

This happens when you set the branch of that site in the dashboard to a value
that does not exist in the repository you just pushed. You need either to fix the
value in the dashboard, or checkout the correct branch, and push again.

[deployment]: /how_it_works/deployment
[boxfile]: /how_it_works/boxfile
[support]: /faq/support
[shellbox]: /getting_started/getting_started-shell
