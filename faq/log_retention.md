---
title: How does freistilbox handle logs?
description: "Web application logs are useful for analytics and troubleshooting. Our managed hosting platform provides you with all the logs you need."
layout: default
parent: FAQ
status: published
categories:
  - tech_faq
tags:
  - log
  - dataprotection
---

# How freistilbox handles log files

## Website logs

Website logs are logs files created by your web applications, specifically access and error logs. These logs are stored in the individual website's shared storage space.

In order to be compliant with regulations such as GDPR, we keep website logs for the duration of 2 weeks. Older website logs get deleted. If you need to archive log files for a longer time, please make sure to download them before their retention time expires.

Since we consider website logs part of your web application data, we don't do any processing on them. If you are required to limit the storage of protected data (such as IP addresses), it is your responsibility to put appropriate measures in place to anonymise this data and/or to delete log files within the stipulated time frame. If you need assistance in implementing these measures, please contact our [support team](/faq/support).

## System logs

System logs are log files created by the hosting infrastructure. These log files are necessary for operating the hosting infrastructure and are not available to customers. We retain system logs for a reasonable duration, usually about two weeks.

Data protected by privacy regulations such as IP addresses stored in these log files gets anonymised within 7 days.
