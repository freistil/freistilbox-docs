---
title: Why does Mysqldump or drush sql-dump fail suddenly?
description: "This article helps you fix to create database dumps with newer Ubuntu versions."
layout: default
parent: FAQ
categories:
  - tech_faq
tags:
  - database
  - mysqldump
  - drush
---

# Creating a database dump fails

When using just `mysqldump db123` or `drush sql-dump` you might see this error
message:

`mysqldump: Couldn't execute 'FLUSH TABLES': Access denied; you need (at least
one of) the RELOAD privilege(s) for this operation (1227)`

With newer versions of `mysqldump` the MySQL team introduced the requirement of
privileges that we can't provide since these provide admin level access in some
cases.

This is why you should adapt your commands as such:

* `mysqldump --no-tablespaces --column-statistics=0`
* `drush sql-dump --extra-dump="--no-tablespaces --column-statistics=0"`


We are looking into making this easier and provide a longterm fix as well.
