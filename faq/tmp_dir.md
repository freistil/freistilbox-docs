---
title: Where can my application save temporary files?
description: "For security reasons, each application on our managed hosting platform gets its own space for temporary files."
layout: default
parent: FAQ
categories:
  - tech_faq
tags:
  - filesystem
---

# Saving temporary files

For security reasons, different website instances on a freistilbox Cluster do
not all use the directory `/tmp`; access to this directory might even be
blocked. Every website instance gets its own directory for temporary files on
the same hierarchical level as the application's `docroot`.

In Drupal, enter "`../tmp`" as the temporary storage directory in the filesystem
configuration (_Administer > Configuration > Media: File system_).
