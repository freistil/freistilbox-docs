---
title: Technical support
description: "DevOps is in our blood. We love working with web developers and we give them direct access to our web operations experts free of charge."
layout: default
parent: FAQ
categories:
  - basics
tags:
  - support
---

# Technical support

Did you encounter an issue that you can't resolve with our [online
documentation](/) alone? Our team is here to help you with any question or
problem.

Sign in to your [freistilbox Dashboard](https://dashboard.freistilbox.com) and
the website in question. From there, you'll be able to reach our engineers via
support request.

To create a support request, simply click the link "Contact support" in the
"Support" section of your website's details page:

![Getting support from the dashboard](/assets/images/dashboard_support_link.png)

This will create a new ticket with our engineering team in which we'll document
our progress to resolution. It will automatically contain important details
like your cluster and website IDs.

{: .important }
**Please do not mix unrelated topics in a single request.** The resolution of
your issues will be much easier to track if you create a separate support
request for each of them.

## Essential details

You can speed up the resolution of an issue by answering the following
questions as precisely as possible:

* **WHAT** are the symptoms of the problem?
* **WHERE** do they manifest?
* **WHEN** was the problem first detected and, if it was only temporary, when
  did it end?

Here is an example support request that covers all the essential details:

> _[WHAT]_ We noticed error messages ("error 503")
> _[WHERE]_ on our website www.example.com. 
> _[WHEN]_ The problem started circa 14:20 CET and at 14:35, the site was
> working okay again.

The following details will help us, too:

* a link to some instructions you followed
* a relevant snippet of code or configuration
* a screenshot of an error message

_Always make sure to give us as much information as possible._ The fewer roundtrips with you we need to get the issue clear, the faster we'll be able to resolve it.
