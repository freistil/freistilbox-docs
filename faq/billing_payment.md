---
title: "Payment methods"
description: "With freistilbox, you can choose the most convenient way to pay your managed hosting bills."
layout: default
parent: FAQ
categories:
  - billing
tags:
  - payment
---

# Payment methods

You can pay your hosting fees monthly via a credit card subscription. We use
[Stripe](https://www.stripe.com) as our credit card processor. In order to be
compliant with PCI/DSS regulations, your credit card details are stored in their
high-security system only.

If paying by credit card is no option for you, you can choose quarterly or
yearly advance billing and pay via bank transfer.

We do not offer direct debit payment. While the introduction of SEPA Direct
Debit made it possible to do intra-EU direct debits, we found it too
bureaucratic and decided not to offer this payment channel.
