---
title: Where can I see the current status of freistilbox?
description: "When something seems to be wrong with our managed hosting platform, here is where you can see what's going on."
layout: default
parent: FAQ
status: published
categories:
  - tech_faq
tags:
  - support
---

# Maintenance and outage notification

You can see the current status of our hosting platform at any time on our status
page, [status.freistilbox.com][1].

To get automatically notified of new status updates, simply click the
"Subscribe" button on the status page.

We recommend also following our [@freistilops][2] Twitter account where you can
send us questions and feedback at any time.

[1]: https://status.freistilbox.com
[2]: https://twitter.com/freistilops
