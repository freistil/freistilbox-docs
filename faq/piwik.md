---
title: How can I install piwik on freistilbox?
description: "How to set up Piwik on freistilbox and what limits you have to expect"
layout: default
parent: FAQ
categories:
  - tech_faq
tags:
  - piwik
---

# Important compatibility note

Due to the nature of freistilbox and the design of Piwik it is not yet possible to fully support Piwik on our infrastructure. Piwik will work and do its job as expected, but you will see error messages complaining that piwik.js is not writeable, and you may experience a few limitations because of this. To be more precise, it means that plugins cannot use the JavaScript tracker.

# How to set up Piwik on freistilbox

## Create a database

You have two options here: either you create a new site just for Piwik, or you create a second database for an existing site. By default, we limit the number of data bases per site to one, but we can raise this limit to allow more. Just [contact our support team][support team].

## Unpack Piwik locally

Clone the git repo of the site you want to install Piwik to and unpack the Piwik tarball or zipfile into the docroot/ directory, but do not yet push. It is important that Piwik is reachable under the /piwik location (e. g., www.domain.com/piwik) as we already have our default Varnish configuration set up in a way to properly handle Piwik.

## Remove config directory from the repository

Parts of Piwik must be writeable and thus reside in the shared storage of freistilbox. These parts must not be checked in via git, but you need to copy them there once *not* using git. So move (not copy!) this directory out of the git repository:

`mv /docroot/piwik/config /tmp`

The files inside that directory need to be copied after you have pushed the repository (don't do that yet), see below.

## Changes to the .gitignore file

Append these lines to your existing .gitignore file:

```plain
/docroot/piwik/tmp
/docroot/piwik/config
```

## Adapt the Boxfile

The [Boxfile][boxfile] configures the shared folders for this site. For Piwik, you need at least these entries:

```yaml
version: 2.0
shared_folders:
  - docroot/piwik/tmp
  - docroot/piwik/config
```

## Create .user.ini

Create a file called `.user.ini` in the docroot/ directory containing at least this:

```ini
always_populate_raw_post_data=-1
```

## Deploy

Commit all changes and deploy them using `git push`.

## Copy config files

Copy the files you moved to `/tmp` before to the shellhost, and there to the correct location. You can do this by issuing:

`rsync -avz /tmp/config/* sSITE-ID@cCLUSTER-IDs.freistilbox.net:current/docroot/piwik/config`

Replace SITE-ID and CLUSTER-ID with the correct values for your site and cluster. The actual command would look like this:

`rsync -avz /tmp/config/* s1234@c123s.freistilbox.net:current/docroot/piwik/config`

## Test

Confirm your installation is working. As mentioned above, the core functionallity will work, but you may encouter error messages saying that piwik.js is not writeable. If you encounter any issues, do not to hesitate [contact our support team][support team].

[support team]: /faq/support
