---
title: Why application code is read-only on freistilbox
description: "Our managed hosting platform allows you to use modern development workflows for updating your web application."
layout: default
parent: FAQ
categories:
  - tech_faq
tags:
  - deployment
  - filesystem
  - staging
---

# Why application code is read-only on freistilbox

If your web application tries to modify its own files, for example in order to
auto-install an update, it will fail.

The reason is that our managed hosting platform needs to maintain consistency of
your application code across _multiple_ application boxes. This consistency
would be compromised if an application process running on a single box would be
allowed to make changes to its local application code only. That's why
application code on freistilbox is read-only. (Your application can of course
create asset files but they are stored separately on a [shared storage
system][filesystem].)

Changes to application code can be deployed exclusively via the application's
Git repository. The freistilbox deployment process will then automatically apply
this update to all your application boxes at the same time.

## How to perform code updates

You simply update your application locally on your dev machine and push the
changes. See our [tutorial][gsdeployment] for details.

From here, you can use [staging environments][staging] to build a continuous
integration workflow.

[staging]: /how_it_works/staging/
[gsdeployment]: /getting_started/getting_started-deployment/
[filesystem]: /how_it_works/filesystem/
