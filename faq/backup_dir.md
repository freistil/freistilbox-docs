---
title: Where can my application put backup files?
description: "If you want to create backups additional to the ones our managed hosting platform makes automatically, you can do so easily."
layout: default
parent: FAQ
categories:
  - tech_faq
tags:
  - backup
  - storage
  - filesystem
---

# Where to store backup files

Anonymous site visitors must not be able to download backups of your website
database. That's why you should use a `private` directory next to (and thus
outside of) your application's `docroot` as a protected assets path.

In your `Boxfile`, declare the "private" directory as permanent storage (see the
[Boxfile][boxfile] documentation for details):

```yaml
version: 2.0
shared_folders:
  - private
```

## Drupal

Configure the protected asset path on the File System page (_Administer >
Configuration > Media: File system_) as "`../private`". Here, the "Backup &
Migrate" module will automatically create a subdirectory named `backup_migrate`
for its backups.

## Access via SSH/SFTP

When you view your assets directory via SSH/SFTP, you'll find your backups below
`current/private`.

[boxfile]: /how_it_works/boxfile
