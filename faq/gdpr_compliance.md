---
title: Data protection
description: "As an EU-based business, we safeguard your data and are fully compliant with EU data protection regulations."
layout: default
parent: FAQ
status: published
categories:
  - basics
tags:
  - dataprotection
  - gdpr
---

# Data protection

freistilbox is fully compliant with the EU General Data Protection Regulation
(GDPR).

Just like our managed hosting, we've also made signing a Data Processing
Agreement simple and straightforward. Simply click on one of the following links
to start the signing process via DocuSign:

* English: [Data Processing 
Agreement](https://eu.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=65bef5b3-6468-49e2-8520-9754f2b15110&env=eu&acct=0a9d6715-f7b3-4baf-ab3b-10cfcde259de)
* German: [Vereinbarung zur 
Auftragsdatenverarbeitung](https://eu.docusign.net/Member/PowerFormSigning.aspx?PowerFormId=7a0791fc-dfc6-41a9-9e59-d5e5c174387e&env=eu&acct=0a9d6715-f7b3-4baf-ab3b-10cfcde259de)

If you have any questions on this topic, please contact
[dataprotection@freistil.it](mailto:dataprotection@freistil.it).
