---
title: Why is the server time running ahead/behind of my own?
description: "This article explains the intricacies of time zones and how our managed hosting platform deals with them."
layout: default
parent: FAQ
status: published
categories:
  - tech_faq
tags:
  - time
  - log
---

# Why freistilbox runs in the UTC time zone

You might have noticed a difference between your local time and the time displayed on our servers. This is intentional.

Not only are our customers distributed over different timezones, our team is, too! So, regardless of which timezone we chose for running our server clocks, it'd be "wrong" for a number of people.

Running different servers in different timezones would not only make things more confusing, it would even make it impossible for us to easily correlate log entries when we have to diagnose errors or other events between servers in our distributed IT architecture.

That's why we run the clocks of all of our servers in the neutral timezone UTC ([Coordinated Universal Time](https://en.wikipedia.org/wiki/Coordinated_Universal_Time)). Since UTC is the primary standard by which the world regulates clocks and time, it's the most simple way of dealing with timezone differences. Everyone only needs to know their local offset to UTC.

Furthermore, the fact that UTC doesn't have the discontinuities caused by daylight savings time (where in spring one hour is lost, and in fall one hour even gets repeated) makes UTC the ideal choice for running server clocks.

This approach is commonly regarded as best practice in timezone setup on servers.
