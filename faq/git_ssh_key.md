---
title: How can I use a separate SSH key for Git?
description: "Our managed hosting platform allows you to use different SSH keys for shell and Git access. This article explains the necessary configuration."
layout: default
parent: FAQ
status: published
categories:
  - tech_faq
tags:
  - git
  - ssh
---

# How to use a separate SSH key for Git

If you have more than one SSH key in use, you can configure SSH to use the right
private key by adding the following block to your SSH configuration in
`~/.ssh/config`:

```
Host repo.freistilbox.net
     Hostname       repo.freistilbox.net
     IdentityFile   ~/.ssh/id_rsa.freistilbox
     IdentitiesOnly yes
```

Simply replace the entry after `IdentityFile` with the actual path to your
private key file.
