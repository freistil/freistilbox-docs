---
title: Where can I get website metadata?
description: "How to access website metadata from web applications and scripts"
layout: default
parent: FAQ
categories:
  - tech_faq
tags:
  - metadata
---

# The site-config.json file and what it is good for

On both web and shell boxes, we provide you with a file called
`site-config.json`. This file contains metadata on your website; i.e. most of
the information you can see on the freistilbox Dashboard, such as cluster ID,
site handle, PHP version, webmaster email address etc. You can make use of its
contents in your web application as well as in scripts and batch jobs run on
your shell box.

The metadata file gets deployed for each site user account in the following
path:

```plain
~/site/current/config/site-config.json
```

You can parse it using any JSON library.

If you have any questions, simply [contact our support team][support]!

[support]: /faq/support
