---
title: "Outgoing IP addresses"
description: "On our distributed hosting platform, your website doesn't run in just one single server."
layout: default
parent: FAQ
categories:
  - tech_faq
tags:
  - address
  - architecture
---

# Outgoing IP addresses

"What is the IP address of my web server?" This question is typically asked in the context of authenticating your website at external services and firewalls. The answer is: _There is no server._

This may sound like a response out of "The Matrix" (who am I kidding, of course it does) but it's true. If we're more precise, there is no _single server_. On freistilbox, your web applications don't run on a single server like with conventional hosting, they run in what we call "web boxes" -- of which there can be any number at a given time.

Here's an architecture diagram of our distributed hosting platform to explain this a bit more. As you can see, incoming HTTP requests pass multiple stages on their way to your web application.

![freistilbox architecture](/assets/images/freistilbox_architecture.png)

1. First, they reach one of the freistilbox edge routers. The address of this server is the only one that is fixed; that's the one you use for your website's DNS entry.
2. The edge router will forward the HTTP request to your Varnish proxy. Varnish will try to respond immediately with cache content.
3. Only if that's not possible, Varnish will pass the request on to one of your web boxes, which will execute your application code to handle it.

Having multiple web boxes per freistilbox cluster has huge advantages for everyone. Our customers enjoy the higher fault tolerance that comes with this kind of redundancy. Scaling the performance of a freistilbox cluster up or down is done in a matter of minutes. Our web operations team, on the other hand, gains a lot of flexibility in keeping the performance and reliability of our managed hosting platform high. We don't have to make huge changes to a runnning server or even cause downtime with a Linux kernel upgrade. Instead, we simply launch new boxes and tear down old ones.

**The downside of this dynamic architecture is that not only is there no single server, the IP addresses from which your website starts outbound connections can also change at any given time.** That's why we are currently not in the position of giving you reliable information about source addresses that you could enter into firewalls or other access management systems.

We're researching ways to solve this problem. In the meantime, we advise against using IP addresses to authenticate your web application in external services. In this age of cloud computing, there are superior alternatives, for example API tokens that you can configure in your web application.
