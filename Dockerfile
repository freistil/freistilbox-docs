FROM ruby:3.3.6

RUN apt-get update -qq && apt-get install -y \
  build-essential \
  nodejs
RUN gem install bundler

RUN mkdir /app
WORKDIR /app

COPY Gemfile /app/Gemfile
COPY Gemfile.lock /app/Gemfile.lock
RUN bundle install

ADD . /app

RUN bundle exec jekyll build

EXPOSE 5000
CMD ["bundle", "exec", "rackup", "-o", "0.0.0.0", "-p", "5000"]
