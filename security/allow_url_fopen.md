---
title: Why is allow_url_fopen() disabled?
description: "Security is an important part of managed hosting. That's why we restrict a few features whose result can be achieved in a more secure way."
layout: default
parent: Security
categories:
  - security
tags:
  - php
---

# Why we disable allow\_url\_fopen

With regards to security, we follow the "Shared Responsibility" principle: our
operations team manages all parts of the hosting platform while our customers
take care of their web applications. The downside of this approach is that since
we don't control the application layer, we can't ensure that security on this
level is as tight as it should be.

One particular security risk is including remote content by using the PHP
function `fopen()` with an external URL. We decided to disable the use of this
function by default.

If you find error messages like the ones below in your logs, then your
application probably tries (unsuccessfully) to include remote content:

* `Warning: fopen() [function.fopen]: URL file-access is disabled in the server
  configuration in /.../ on line (...)`
* `Warning: file_get_contents() [function.file-get-contents]: URL file-access is
  disabled in the server configuration in /.../ on line (...)`
* `Warning: include() [function.include]: URL file-access is disabled in the
  server configuration in /.../ on line (...)`
* `Warning: getimagesize() [function.getimagesize]: URL file-access is disabled
  in the server configuration in /.../ on line (...)`
* `Warning: readfile() [function.readfile]: URL file-access is disabled in the
  server configuration in /.../ on line (...)`

In this case, please take a look at the following references. There are better
-- i.e. more secure -- alternatives to using `fopen()` with an external URL, for example the `Request` class in Symfony.
