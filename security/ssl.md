---
title: SSL encryption
description: "Security is a key aspect of our managed hosting platform, and SSL support is included free of charge."
layout: default
status: published
parent: Security
categories:
  - security
tags:
  - ssl
  - htaccess
---

# SSL encryption

The freistilbox edge routers decrypt incoming SSL requests right when they
reach our managed hosting platform. This practice is called "SSL offloading"
and has several advantages:

* Since the freistilbox application boxes do not need to spend computing
  capacity on SSL, they have more resources available to run your web
  application.
* Content caching works for encrypted and unencrypted requests. This saves even
  more capacity on the application boxes and avoids that you have to resort to
  "mixed mode" for fast asset delivery.

## Using free TLS certificates from Let's Encrypt

Our new generation of edge routers not only speeds up your content delivery by
way of 10 Gbit/s uplinks and HTTP/2, they also allow you to secure your web
traffic free of charge with TLS certificates from Let's Encrypt. See our [Let's
Encrypt documentation](/security/lets_encrypt) for details.

## Using Custom TLS certificates

Since it exclusively provides domain-validated certificates, Let's Encrypt can
only cover basic web security needs. If your security requirements ask for an
owner-validated certificate or you'd like to use a wildcard certificate to
secure a growing list of subdomains ("*.example.com"), we can provide you with
a variety of individual TLS certificates.

## How to handle secure requests in your application

Because SSL requests are decrypted by our edge routers before they reach one of
your application boxes, your web application will always receive plain HTTP
requests. In order to be able to tell which requests originally came in
encrypted, our edge routers mark them with the HTTP header `X-Forwarded-Proto:
https`.  
When our application boxes see this HTTP header, they set an environment
variable named `HTTPS` to the value `on`.

In your application, you can test this variable to see if a request came over
an encrypted connection.

In PHP:

```php
if ($_SERVER['HTTPS'] == "on")
```

This variable is set by PHP for received SSL requests, too, so existing
applications, plugins and modules should work out of the box with our
configuration.

{: .notice }
There is a catch with testing for SSL in an `.htaccess` file. While Apache's
`mod_rewrite` has a built-in condition named `HTTPS`, its result is true only
if the request actually reached the box in its encrypted form -- which will
never be the case for the reasons explained above. Therefore, you have to check
the _environment variable_ of the same name instead.

This will _not_ work:

```apache
RewriteCond %{HTTPS} on
```

This will work:

```apache
RewriteCond %{ENV:HTTPS} on
```

We recommend you cover all bases by checking both conditions:

```apache
RewriteCond %{HTTPS} on [OR]
RewriteCond %{ENV:HTTPS} on
```

## Redirecting all insecure requests to HTTPS

To force HTTPS for all requests, use the following snippet in `.htaccess`:

```apache
RewriteCond %{HTTPS} !on
RewriteCond %{ENV:HTTPS} !on
RewriteRule ^(.*)$ https://%{HTTP_HOST}/$1 [R=301,L]
```
