---
title: How can I limit website access to a number of IP addresses?
description: "This article explains how freistilbox allows you to limit access to your website to a few IP addresses."
layout: default
parent: Security
categories:
  - security
tags:
  - apache
---

# How to limit website access to a number of IP addresses

If you want to limit access to your website instance to a small number of IP
addresses, for example to only allow direct origin access for your CDN only,
you can use the `.htaccess` file. This file changes the behaviour of the
Apache web server we use on our application boxes. See the
[Apache htaccess documentation](https://httpd.apache.org/docs/2.4/howto/htaccess.html)
for details.

In the Apache documentation, you'll find that this is how you restrict access to a
website running on a single Apache server:

```apache
<RequireAny>
  Require ip 1.2.3.4 2.3.4.5
</RequireAny>
```

But because your application boxes don't get their requests directly from your
visitors but via our Edge Routers and the Content Cache/Load Balancer, this will
not work. The sender will always appear to be one of the Load Balancer nodes.

You can get the actual visitor IP address from the HTTP header `X-Forwarded-For`
which is set by our Edge Routers. By evaluating this header, you can grant access
based on an environment variable that you set if the header contains one of
the allowed origin addresses. Here's a configuration snippet that implements
this approach:

```apache
SetEnvIf X-Forwarded-For "1.2.3.4" AllowIP
SetEnvIf X-Forwarded-For "2.3.4.5" AllowIP
<IfModule mod_authz_core.c>
  <RequireAny>
    Require env AllowIP
    Require ip 1.2.3.4 2.3.4.5
  </RequireAny>
</IfModule>
```

As you can see, the snippet still implements the default Apache way using `Require ip` as well.
We recommend including this line to be compatible with future improvements of how freistilbox
handles address information.
